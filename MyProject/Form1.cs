﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyProject
{
    public partial class Form1 : Form
    {
        private Game g;
      

        public Form1()
        {   
            InitializeComponent();
            g = new Game();
            g.DoReset(tbPanel);
        }

        private void lb_Click(object sender, EventArgs e)
        {
            Label clickedLabel = sender as Label;
            g.DoAnswer(clickedLabel);

        }
    }
}
