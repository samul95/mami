﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MyProject
{
    class Game
    {
        Random random = new Random();
        Label firstClicked = null;
        Label secondClicked = null;
        Timer MainTime = new Timer();
        TableLayoutPanel table = null;



        List<string> icon = new List<string>()
        {
            "!", "!", "N", "N", ",", ",", "k", "k",
            "b", "b", "v", "v", "w", "w", "z", "z"
            //,"t", "t", "r", "r", "q", "q", "m", "m"
        };


        public void DoReset(TableLayoutPanel TablePanel)
        {
            MainTime.Tick += MainTime_Tick;
            table = TablePanel;

            foreach (Control control in table.Controls)
            {
                Label iconLabel = control as Label;
                if (iconLabel != null)
                {
                    int randomNumber = random.Next(icon.Count);
                    iconLabel.Text = icon[randomNumber];
                    iconLabel.ForeColor = iconLabel.BackColor;
                    icon.RemoveAt(randomNumber);
                }
            }
        }

        private void MainTime_Tick(object sender, EventArgs e)
        {
            MainTime.Stop();
            firstClicked.ForeColor = firstClicked.BackColor;
            secondClicked.ForeColor = secondClicked.BackColor;
            firstClicked = null;
            secondClicked = null;
        }


        public void DoAnswer(Label clickedLabel)
        {
            if (MainTime.Enabled == true)
                return;

            if (clickedLabel != null)
            {

                if (clickedLabel.ForeColor != clickedLabel.BackColor)
                    return;

                if (firstClicked == null)
                {
                    firstClicked = clickedLabel;
                    firstClicked.ForeColor = Color.Black;
                    return;
                }

                secondClicked = clickedLabel;
                secondClicked.ForeColor = Color.Black;

                if (firstClicked.Text == secondClicked.Text)
                {
                    firstClicked = null;
                    secondClicked = null;
                    CheckWin();
                    return;
                }

                MainTime.Start();
            }

        }
        private void CheckWin()
        {
            
            foreach (Control control in table.Controls)
            {
                Label iconLabel = control as Label;

                if (iconLabel != null)
                {
                    if (iconLabel.ForeColor == iconLabel.BackColor)
                        return;
                }
            }

           MessageBox.Show("Победа!");

        }

    }
}
